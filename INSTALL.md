# Starting from scratch

In order to buld a cluster, you are going to need a working master. This is pretty much just a gateway with some extra
functionality.

You will need to get working:
* Network routing
* A DHCP server
* A DNS server
* A TFTP server
* A DNS server (optionally)
* A webserver
* An rsync server AND/OR a bittorrent server (TBD!)
* Buildroot

In this example build I will walk you through installation on an Ubuntu 20.04 machine. This should thus hold as accurate
for Debian installations, but EL users will need to adjust things accordingly.

## Base Networking
Ubuntu 20.04 is a bit of an odd duck - now using cloud-init and netplan for network deployment in order to abstract away
NetworkManager/systemd-network/dnsmasq/systemd-resolve/etc. You will need to alter your deployment according to your
situation.

For me, I have two interfaces - ens18 to the outside world (and DHCP), and ens19 to the cluster. Thus, I set up a static
IP on ens19, and leave DHCP on ens18 via creating a file at `/etc/netplan/99-myconf.yml` and running `netplan apply`. If
you are following this guide, you'll likely come back here.

You will also need to allow nodes to reach the internet - this can be done by allowing forwarding in the kernel by
setting `net.ipv4.ip_forward=1` in sysctl (hint: Exit /etc/sysctl.conf and reload procps), and then forwarding packets
being sent through the master to the MASQUERADE handler. The simplest means of doing this is with:

`iptables -A POSTROUTING -o ens18 -j MASQUERADE`

And then saving this with iptables-persistent. You might want to use a firewall wrapper for this purpose, such as ufw
or shorewall - that exercise is left to the reader.

## TFTP (tftpd-hpa)
The most standalone component to set up is the TFTPd. I recommend tftpd-hpa - it works best with systemd, as atftpd
seems to rely on inetd, which is a bit of an extra fuss, given that systemd can also spawn stuff on response to sockets
with a .socket file. Configuration of this service is done through `/etc/default/tftpd-hpa`, which is re-read on restart.

In this example, we will be changing the path from /srv/tftp to /tftpboot for simplicity with regards to backwards
compatibility. Use the example file in examples/default.tftpd-hpa, and make sure this path exists and is readable by the
tftp user. I have increased the verbosity so you can see failed connections more easily for debugging purposes.

Most of the rest of the files will be put into /tftpboot.

## GRUB (EFI PXE)

Start by installing the precompiled EFI binaries for grub from your distro. You could compile them from scratch here
should you feel the need, but there's no requirement to use anything but stock:
`apt-get install -y grub-efi-amd64 grub-efi-amd64-bin grub-efi-amd64-signed grub-imageboot grub-pc-bin grub2-splashimages shim shim-signed`

The advantage of taking this route is gaining the ability to run the command:
`grub-mknetdir --net-directory=/tftpboot`

Which will automatically create the folder /tftpboot/boot/ and all of the subdirectories nicely for you, structured as:
* boot/grub/x86_64-efi (for the 64 bit EFI binaries and .mod files)
* boot/grub/i386-pc (for the 32 bit .mod files)
* boot/grub/fonts (for the fonts)
It will also instruct you to use `/tftpboot/boot/grub/x86_64-efi/core.efi` as the binary to boot from. What it will not
tell you is that it will also read its config from `/tftpboot/boot/grub.cfg-XXXXXXXX` where the X's are hex values for
the IPv4 IP address, or `/tftpboot/boot/grub.cfg-YY-YY-YY-YY-YY-YY` Where the Y's are the lowercase MAC address for the
interface requesting config. To maintain consistency with the BIOS version for pxeconfig, we will be using the IP based
config files. You can find an example of such a config in the examples directory, as `examples/grub.cfg.example`.

## PXELinux (BIOS PXE)

(TODO - this one isn't rock solid on detail)

Start by installing the precompiled PXELinux binaries for grub from your distro. You could compile them from scratch here
should you feel the need, but there's no requirement to use anything but stock:
`apt-get install -y pxelinux syslinux`
You'll also need the syslinux mod files in order for pxelinux to start properly. From here, it's just a matter of
copying the binaries and modules into the /tftpboot folder so they can be loaded remotely:
`cp /usr/lib/PXELINUX/pxelinux.0 /tftpboot/`
`cp -r /usr/lib/syslinux/modules/bios/ /tftpboot/syslinux`
You'll also need to `mkdir /tftpboot/pxelinux.cfg` - this is where pxelinux will pull its configs from. It uses
`default` by default, but will also look for a file called `XXXXXXXX` where the X's are hex values for the IPv4 IP
address requesting config. You can find an example of such a config in the examples directory.

## DHCP (isc-dhcp-server)
ISC DHCP server is the oldest, most commonly used DHCP server from Linux. It has lots of documentation, and is available
on many distros. Install this, and alter /etc/dhcp/dhcpd.conf for your cluster. I have an internal IP range of
192.168.254.0/24 on eth1, so I set this up as per the example file at dhcpd.conf.example.

Note the additional option sali-imgsrv on option 224. This is used to transfer the location of the image server to the
sali installer so it can find the master script.

The architecture option (93) is also added here - this option is sent back to the server from the client as per RFC4578
and allows the DHCP server to switch from BIOS to PXE images on the fly.

One thing you should consider in a mature cluster is assigning your nodes a static IP over DHCP. This can be done in this
file by updating and altering the statically assigned block. You might consider putting this somewhere else and
automating this.

## DNS (bind9)
Bind9 is a venerable DNS server. Having an internal DNS config makes everything much more human-readable and easier to
debug. It can also be tied to the DHCP server by sharing secret keys, which makes DNS updates dynamic, or a manual
config can be applied.

On Ubuntu, there is a separated /etc/bind/named.local and /etc/bind/named.options file. I like to set up my database
references in named.local, and then overload named.options with new additions. DNSSEC is pretty broken right now, so
that one is disabled in the example options. I've also provided an example of DNS forwarding to an upstream DNS server
for internet-wide names, and also an example of just one sub-zone that is local.

If you are using pxeconfig, you can use this to display nice names using `hexls` when exploring the pxeconfig
directories.

Once you have your DNS set up, don't forget to come back to your network config and make sure your master is using this
server, otherwise you won't be able to see your internal names at all. In netplan, this is done with a `dhcp4-override`
statement (20.04 only - breaks on 18.04).

## Webserver (apache2)
Apache2 is an enormous beast of a webserver and is capable of being configured to do very many things, thus is a go-to
webserver engine for most. You will want to install:
`apt-get -y install apache2 openssl`
and then
`a2enmod ssl`
As SALI only talks to https connections. By default apache2 binds to all interfaces - you might want to tighten that up in
`/etc/apache2/ports.conf` for your scenario. You should also `a2dissite 000-default` and `a2ensite default-ssl` to switch to
SSL only, and `echo '' > /var/www/html/index.htm` for security. Lastly, `mkdir -p /var/www/html/sali/scripts/` before copying
in the example master_script for booting.
This master_script is used for configuring the partitioning of the machine, as well as setting the source of the image used, be
it rsync or torrent.

## Rsync server (TODO)
rsync is a powerful tool for synchronising filesystems, using a generic interface for backend communications. By default it uses
ssh, but it can also use its own rsync protocol and a daemon, which is what we will be using here. Set this up by editing
`/etc/rsyncd.conf` - you'll find an example in examples - and start and enable the rsync service. What's important is that the
section under images exists and points to the location of your client image.
A good test is if `rsync -rtn rsync://localhost` returns the same list of images you've written into the config file.

## Buildroot (SALI)
Buildroot is a system for building embedded Linux images in a simple, reproducible, and scriptable way. SALI uses buildroot to
build an installation environment to deploy and reconfigure the system disks, and copy the operating system over to the target.
To install this, you will need to download buildroot from `https://buildroot.org/downloads/buildroot-2021.02.1.tar.gz`. Unpack
it, and then cd into the directory.
Buildroot is designed to be able to take custom configs - it does this with the BR2_EXTERNAL variable when compiling. In order to
enable the build for sali, you need to:
```
make BR2_EXTERNAL=<sali-install-path>/buildroot sali_x86_64_defconfig
make -j $(nprocs)
```

This will (eventually) produce a linux kernel in output/images called `bzImage` and a compressed rootfs called
`rootfs.cpio.lzma`. Copy these to your /tftpboot/sali directory to use for booting over TFTP.

## pxeconfig
SALI runs well in combination with pxeconfig, allowing it to be selectively triggered and disabled from commands on the master
node. pxeconfig runs typicalyl through inetd/xinetd or via systemd as a socket-triggered service in order to selectively remove
symlinks to IP-linked configurations. Once you set it up, you will probably want to alter the pxe_config_dir/uefi_config_dir
paths in /usr/local/etc/pxeconfig.conf to match the paths used by SALI.

## SALI scripts
Once SALI has its master_script via HTTPS, it will also try and download extra scripts over rsync from the `scripts` location on
the image server. Here, you can customise your master_script with extra commands as necessary for specific times, such as before
installation, after installation, and before reboot. Of importance here is to dial home and allow pxeconfig to know that
installation was successful, and to not cause the client to reinstall next time. You might also want to do some extra commands
here in the new target filesystem so that it can, for example, correctly locally boot, or perform some sort of external
authentication.
The default location for this is /data/sali/scripts. Use this location and work from the example scripts provided.

## Finishing up
* Make sure you have your config files in the right places (/tftpboot/pxelinux.cfg or /tftpboot/boot/grub)
* Use pxeconfig to link the client IP to a specific target
* Get that target to boot via PXE from your DHCP server
* Watch the magic happen
